//Clear slider button text
$(document).ready(function(){
  $('.slideshow-prev').text('')
  $('.slideshow-next').text('')
})

// Dropdown
$('#switch').click(function(e){
  e.preventDefault()
  $('#text-dropdown').slideToggle(200)
})

//Flashcard click with another button
$('#button-card-one').click(function(){
  $('#btn-one-none').click()
})

$('#button-card-two').click(function(){
  $('#btn-two-none').click()
})

$('#button-card-three').click(function(){
  $('#btn-three-none').click()
})

//Highlight selected answer in activities
$('#radio-1').on('change', function(){
  $('#label-1').css('color', '#4070A7')
  $('#label-2').css('color', '#484E52')
  $('#label-3').css('color', '#484E52')
})
$('#radio-2').on('change', function(){
  $('#label-2').css('color', '#4070A7')
  $('#label-1').css('color', '#484E52')
  $('#label-3').css('color', '#484E52')
})
$('#radio-3').on('change', function(){
  $('#label-3').css('color', '#4070A7')
  $('#label-1').css('color', '#484E52')
  $('#label-2').css('color', '#484E52')
})

//Close Arrow Accordion
$('#acco-close-1').click(function(e){
  e.preventDefault()
  $('.title-1').click()
})
$('#acco-close-2').click(function(e){
  e.preventDefault()
  $('.title-2').click()
})
$('#acco-close-3').click(function(e){
  e.preventDefault()
  $('.title-3').click()
})
